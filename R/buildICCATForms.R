#' Build a ICCAT Task 1 - NC report on a CL dataset in COST format.
#'
#' @param costClObj COST CL object.
#' @param areaField field with the area code.
#'
#' @return file path of the created report.
#'
#' @examples
#' buildICCAT_T1_NC_2015(myCostCL)
#'
#' @author Norbert Billet - IRD
#'
#' @export
buildICCAT_T1_NC_2015 <- function(costClObj, areaField="area") {

  checkXlsx()

  if (! inherits(costClObj, "clData")) {
    stop("costClObj must be a COST clData object")
  }

  resultDf <- NULL
  for (currTaxon in na.omit(unique(costClObj@cl$taxon))) {
    # currCl <- subset(costClObj, taxon==currTaxon, table="cl")
    currCl <- subset(costClObj@cl, taxon==currTaxon)
    currCl$gear <- unlist(lapply(strsplit(currCl$foCatEu6, "_", fixed=TRUE), function(x) x[[1]]))
    currCl$originalArea <- currCl[, c(areaField)]

    for (currGear in unique(currCl$gear)) {
      currIccatSpp <- NULL
      if (currTaxon == "Katsuwonus pelamis") {
        # for SKJ, all gears
        outputAreaTypeSampling <- "ICCAT:SAMPLING:SJ"
        outputAreaTypeStock <- "ICCAT:STOCK:SKJ"
        currIccatSpp <- "SKJ"
      }
      if (currTaxon == "Xiphias gladius") {
        # for SWO, all gears
        outputAreaTypeSampling <- "ICCAT:SAMPLING:BIL"
        outputAreaTypeStock <- "ICCAT:STOCK:SWO"
        currIccatSpp <- "SWO"
      }
      if (currTaxon == "Thunnus thynnus") {
        # for BET, all gears
        outputAreaTypeSampling <- "ICCAT:SAMPLING:BF"
        outputAreaTypeStock <- "ICCAT:STOCK:BFT"
        currIccatSpp <- "BFT"
      }
      if (! is.null(currIccatSpp)) {
        message("Processing for species: [", currTaxon, "] and gear: [", currGear, "]")
        df <- as.data.frame(subset(currCl, gear==currGear) %>% group_by(vslFlgCtry, harbour, year, originalArea) %>% summarise(landWt=sum(landWt, na.rm=TRUE)))

        df$areaType <- getAreaType(df$originalArea)
        df <- df[! is.na(df$areaType),]
        df$iccatStock <- areaMapping(area = df$originalArea, inputType = df$areaType, outputType = outputAreaTypeStock)
        df <- df[! is.na(df$iccatStock),]
        df$iccatArea <- areaMapping(area = df$originalArea, inputType = df$areaType, outputType = outputAreaTypeSampling)
        df <- df[! is.na(df$iccatArea),]
        df <- as.data.frame(df %>% group_by(vslFlgCtry, harbour, year, iccatStock, iccatArea) %>% summarise(landWt=sum(landWt, na.rm=TRUE)))
        if (nrow(df) > 0) {
          df$iccatSpp <- currIccatSpp
          df$gear <- currGear
          if (is.null(resultDf)) {
            resultDf <- df
          } else {
            resultDf <- rbind(resultDf, df)
          }
        }
      } else {
        message("Export to ICCAT T1 NC not yet implemented for species: [", currTaxon, "] and gear: [", currGear, "]")
      }
    }
  }

  if (! is.null(resultDf) & nrow(resultDf) > 0) {
    finalDf <- data.frame(vslFlgCtry=resultDf$vslFlgCtry,
                          harbour=resultDf$harbour,
                          year=resultDf$year,
                          iccatSpp=resultDf$iccatSpp,
                          iccatStock=resultDf$iccatStock,
                          iccatArea=resultDf$iccatArea,
                          taskArea=NA,
                          gear=resultDf$gear,
                          fishingZone=NA,
                          landWt=resultDf$landWt,
                          stringsAsFactors = FALSE)
    finalDf <- finalDf[order(finalDf$vslFlgCtry, finalDf$harbour, finalDf$year, finalDf$iccatSpp, finalDf$iccatStock, finalDf$iccatArea, finalDf$gear),]

    blanckXlsxPath <- system.file("ICCAT", "ST02-T1NC_2015a.xlsx", package="COSTio2", mustWork = TRUE)

    newXlsxPath <- tempfile(fileext = ".xlsx")

    wb <- loadWorkbook(blanckXlsxPath)
    sheets <- getSheets(wb)

    targetSheet <- which(names(sheets) == "ST02-T1NC")

    addDataFrame(finalDf, sheets[[targetSheet]], col.names = FALSE, row.names = FALSE, startRow = 26, startColumn = 1)

    saveWorkbook(wb, newXlsxPath)
    message("File created: [", newXlsxPath, "].")
  } else {
    message("Nothing to process.")
  }
}

#' Build a ICCAT Task 2 - CE report on a CL and CE dataset in COST format.
#'
#' @param costClObj COST CL object.
#' @param costCeObj COST CE object.
#'
#' @return file path of the created report.
#'
#' @examples
#' buildICCAT_T2_CE_2015(myCostCL, myCostCe)
#'
#' @author Norbert Billet - IRD
#'
#' @export
#' @importFrom tidyr unite spread
buildICCAT_T2_CE_2015 <- function(costClObj, costCeObj, areaField="area") {

  checkXlsx()

  if (! inherits(costClObj, "clData")) {
    stop("costClObj must be a COST clData object")
  }

  if (! inherits(costCeObj, "ceData")) {
    stop("costClObj must be a COST ceData object")
  }

  keepSppIccat <- c("BFT", "ALB", "BET", "SKJ", "YFT", "SWO", "BUM", "WHM", "SAI", "SPF", "BSH", "POR", "SMA")
  keepSppSc <- asfisToScientificName(keepSppIccat)

  # CL subset & aggregation
  costClObj@cl$theArea <- costClObj@cl[, c(areaField)]
  costClObj@cl <- subset(costClObj@cl, taxon %in% keepSppSc)
  costClObj@cl$iccatSpp <- scientificNameToAsfis(costClObj@cl$taxon)
  costClObj@cl$gear <- unlist(lapply(strsplit(costClObj@cl$foCatEu6, "_", fixed=TRUE), function(x) x[[1]]))
  aggCl <- as.data.frame(costClObj@cl %>%
                           unite(spp_school, iccatSpp, foCatNat) %>%
                           group_by(vslFlgCtry, harbour, year, month, gear, theArea, spp_school) %>%
                           summarise(landWt=sum(landWt, na.rm=TRUE)) %>%
                           spread(spp_school, landWt))

  # CE aggregation
  costCeObj@ce$theArea <- costCeObj@ce[, c(areaField)]
  costCeObj@ce$gear <- unlist(lapply(strsplit(costCeObj@ce$foCatEu6, "_", fixed=TRUE), function(x) x[[1]]))
  aggCe <- as.data.frame(costCeObj@ce %>%
                           group_by(vslFlgCtry, harbour, year, month, gear, theArea) %>%
                           summarise(foNum=sum(foNum, na.rm=TRUE), daysAtSea=sum(daysAtSea, na.rm=TRUE)))


  agg <- merge(aggCe, aggCl, all.x = TRUE, all.y = TRUE)

  # add square type
  agg$squareType <- NA
  ind <- substr(agg$theArea, 1, 1) == "5"
  agg$squareType[ind] <- "1x1"
  ind <- substr(agg$theArea, 1, 1) == "6"
  agg$squareType[ind] <- "5x5"

  # quadrant
  agg$quadrant <- NA
  ind <- substr(agg$theArea, 2, 2) == "1"
  agg$quadrant[ind] <- "NE"
  ind <- substr(agg$theArea, 2, 2) == "2"
  agg$quadrant[ind] <- "SE"
  ind <- substr(agg$theArea, 2, 2) == "3"
  agg$quadrant[ind] <- "SW"
  ind <- substr(agg$theArea, 2, 2) == "4"
  agg$quadrant[ind] <- "NW"
  # LAT & LON
  agg$lat <- substr(agg$theArea, 3, 4)
  agg$lon <- substr(agg$theArea, 5, 7)

  agg <- agg[, -which(names(agg) %in% c("theArea"))]
  # effort Type 1
  agg$effType1 <- "NO.SETS"

  # effort Type 1
  agg$effType2 <- "D.AT.SEA"

  myNames <- c("vslFlgCtry", "harbour", "year", "month", "gear", "squareType", "quadrant", "lat", "lon", "effType1", "foNum", "effType2", "daysAtSea")

  finalDf <- agg[, myNames]
  finalDf <- cbind(finalDf, agg[, setdiff(names(agg), myNames)])
  if (! is.null(finalDf) & nrow(finalDf) > 0) {
    blanckXlsxPath <- system.file("ICCAT", "ST03-T2CE_2015a.xlsx", package="COSTio2", mustWork = TRUE)

    newXlsxPath <- tempfile(fileext = ".xlsx")

    wb <- loadWorkbook(blanckXlsxPath)
    sheets <- getSheets(wb)

    targetSheet <- which(names(sheets) == "ST03-T2CE")

    addDataFrame(finalDf, sheets[[targetSheet]], col.names = TRUE, row.names = FALSE, startRow = 27, startColumn = 1)

    saveWorkbook(wb, newXlsxPath)
    message("File created: [", newXlsxPath, "].")
  } else {
    message("Nothing to process.")
  }
}

# buildICCAT_T1_NC_2015 <- function(costClObj, areaField="rect", areaType="CWPSquare") {
#
#   checkXlsx()
#
#   if (! inherits(costClObj, "clData")) {
#     stop("costClObj must be a COST clData object")
#   }
#
#   # aggregate CL data
#   df <- COSTaggregate(costObj = costClObj,
#                           statField = "landWt",
#                           strata=list("vslFlgCtry", "harbour", "taxon", "year", "foCatEu6", areaField),
#                           aggFun = sum, na.rm=TRUE)
#
#   # extract gear from metier
#   df$gear <- unlist(lapply(strsplit(df$foCatEu6, "_", fixed=TRUE), function(x) x[[1]]))
#
#   # spatial zone are by species
#   data("spAreas", envir=environment())
#
#   resultDf <- NA
#   for (currTaxon in na.omit(unique(df$taxon))) {
#     currDf <- subset(df, taxon == currTaxon)
#
#     ###### Process data area
#     currDfSp <- NULL
#
#     if (areaType == "CWPSquare") {
#       currDfSp <- getSpDfFromGrid(currDf, currDf[, c(areaField)])
#     }
#
#     if (is.null(currDfSp)) {
#       message("Export to ICCAT T1 NC not yet implemented for area type: ", areaType)
#     }
#
#     ###### Proces spp area
#     areaSpDf <- NULL
#
#     if (currTaxon == "Katsuwonus pelamis") {
#       areaSpDf <- ICCAT.SKJ.areas
#       stockSpDf <- ICCAT.SKJ.stocks
#       currDf$iccatSpp <- "SKJ"
#     }
#
#     if (currTaxon == "Thunnus thynnus") {
#       areaSpDf <- ICCAT.BFT.areas
#       stockSpDf <- ICCAT.BFT.stocks
#       currDf$iccatSpp <- "BFT"
#     }
#
#     if (is.null(areaSpDf)) {
#       message("Export to ICCAT T1 NC not yet implemented for species: ", currTaxon)
#     }
#
#     if (! is.null(areaSpDf) & ! is.null(currDfSp)) {
#       res <- gContains(areaSpDf, currDfSp, byid = TRUE)
#       resRowSum <- rowSums(res)
#       bad <- resRowSum != 1
#       if (sum(bad) > 0) {
#         message("Following rect code non matching: ", paste0(unique(currDf$rect[bad]), collapse = ", "))
#         currDf <- currDf[! bad,]
#         currDfSp <- currDfSp[! bad,]
#         res <- res[! bad, ]
#       }
#       if (nrow(currDf) > 0) {
#         currDf$iccatArea <- areaSpDf$id[apply(res, 1, which)]
#
#         res <- gContains(stockSpDf, currDfSp, byid = TRUE)
#         resRowSum <- rowSums(res)
#         bad <- resRowSum != 1
#         if (sum(bad) > 0) {
#           message("Following rect code non matching: ", paste0(unique(currDf$rect[bad]), collapse = ", "))
#           currDf <- currDf[! bad,]
#           res <- res[! bad, ]
#         }
#         if (nrow(currDf) > 0) {
#           currDf$iccatStock <- stockSpDf$id[apply(res, 1, which)]
#           if (is.na(resultDf)) {
#             resultDf <- currDf
#           } else {
#             resultDf <- rbind(resultDf, currDf)
#           }
#         }
#       }
#     }
#   }
#
#   if (! is.na(resultDf) & nrow(resultDf) > 0) {
#     aggDf <- resultDf %>%
#       group_by(vslFlgCtry, harbour, year, gear, iccatSpp, iccatStock, iccatArea) %>%
#       summarize(landWt=sum(x, na.rm=TRUE))
#     aggDf <- as.data.frame(aggDf)
#
#     finalDf <- data.frame(year=aggDf$year,
#                           harbour=aggDf$harbour,
#                           year=aggDf$year,
#                           iccatSpp=aggDf$iccatSpp,
#                           iccatStock=aggDf$iccatStock,
#                           iccatArea=aggDf$iccatArea,
#                           taskArea=NA,
#                           gear=aggDf$gear,
#                           fishingZone=NA,
#                           landWt=aggDf$landWt,
#                           stringsAsFactors = FALSE)
#
#     blanckXlsxPath <- system.file("ICCAT", "ST02-T1NC_2015a.xlsx", package="COSTio2", mustWork = TRUE)
#
#     newXlsxPath <- tempfile(fileext = ".xlsx")
#
#     wb <- loadWorkbook(blanckXlsxPath)
#     sheets <- getSheets(wb)
#
#     targetSheet <- which(names(sheets) == "ST02-T1NC")
#
#     addDataFrame(finalDf, sheets[[targetSheet]], col.names = FALSE, row.names = FALSE, startRow = 26, startColumn = 1)
#
#     saveWorkbook(wb, newXlsxPath)
#     message("File created: [", newXlsxPath, "].")
#   } else {
#     message("Nothing to process.")
#   }
# }
