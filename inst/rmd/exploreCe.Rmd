---
title: "`r title`"
author: "`r author`"
date: "`r currDate`"
output: pdf_document
---

```{r setup, include=FALSE}
library(dplyr)
library(ggplot2)

knitr::opts_chunk$set(echo = FALSE)


existError <- FALSE

ceTableDim <- function (x, col.name, data) {
  if (missing(data)) {
    data <- costObj@ce
  }
  
  tab <- data %>% 
    group_by_(x) %>% 
    summarise(n=n(), daysAtSea=sum(daysAtSea, na.rm=TRUE))
  tab <- as.data.frame(tab)
  knitr::kable(tab, row.names = FALSE, col.names = c(col.name, "Num. of row", "Days at sea"), caption = paste0("Summary for the \"", col.name, "\" dimension"), digits = 2)
}

ceHeatmap <- function (x1, x2, data) {
  if (missing(data)) {
    data <- costObj@ce
  }
  tab <- data %>% 
    group_by_(x1, x2) %>% 
    summarise(n=n(), daysAtSea=sum(daysAtSea, na.rm=TRUE))
  tab <- as.data.frame(tab)
  if (nrow(tab) < 2) {
    print("Nothing to print\n")
  } else {
    tab$n <- tab$n / sum(tab$n, na.rm=TRUE)
    tab$daysAtSea <- tab$daysAtSea / sum(tab$daysAtSea, na.rm=TRUE)
    newTab <- rbind(cbind(var="n", tab[, c(x1, x2)], value=tab$n),
                    cbind(var="daysAtSea", tab[, c(x1, x2)], value=tab$daysAtSea))
    newTab$var <- factor(newTab$var, labels = c("Num. of row", "Days at sea"))
    newTab[, c(x1)] <- factor(newTab[, c(x1)])
    newTab[, c(x2)] <- factor(newTab[, c(x2)])
    newTab$value <- log(newTab$value * 100)
    newTab$value[is.infinite(newTab$value)] <- min(newTab$value[! is.infinite(newTab$value)], na.rm=TRUE) 
    ran <- range(newTab$value, na.rm = TRUE)
    p <- ggplot(data = newTab) + 
      geom_tile(mapping = aes_string(x=x1, y=x2, fill="value")) +
      facet_grid(~var) +
      scale_fill_gradient(low="yellow", high="blue", na.value="grey25", breaks=seq(from=ran[1], to=ran[2], length.out = 3), labels=c("-", "", "+"), name="") +
      # scale_fill_gradientn(colours=topo.colors(7), na.value="grey50", breaks=c(0,5,10), labels=c("Minimum",0.5,"Maximum"), limits=c(0,1)) + 
      theme_bw() +
      theme(axis.text.x=element_text(angle=45, hjust = 1)) +
      xlab(NULL) + ylab(NULL)
    print(p)
  }
}
```

# COST description

```{r desc, results="asis"}
cat(paste0("Class: ", class(costObj), "\n\n"))
cat(paste0("Description: ", costObj@desc, "\n\n"))
cat(paste0("Number of rows: ", nrow(costObj@ce), "\n\n"))
```

# Dimensions

## Flag Country
```{r dim_vslFlgCtry, results="asis"}
ceTableDim("vslFlgCtry", col.name = "Vessel flag country")
```

## Year
```{r dim_year, results="asis"}
ceTableDim("year", col.name = "Year")
```

## Metier
```{r dim_foCatEu6, results="asis"}
ceTableDim("foCatEu6", col.name = "Metier level 6")
```

## Metier vs year

```{r dim_year_focatEu, fig.width=8}
ceHeatmap("year", "foCatEu6")
```

## Metier vs Ocean

```{r dim_year_taxon, fig.width=8}
costObj@ce$ocean <- areaToOcean(costObj@ce$area)
# temp
costObj@ce$ocean[costObj@ce$area == "GSA 7"] <- "MED"

ceHeatmap("foCatEu6", "ocean")
```

